<xml>
<chapter>Tea in the Heather</chapter>

{190} “May I see it?” Begw said to her mother, and got up on the chair in the milkshed on her knees.

The “it” was jelly, a brand-new thing to Begw’s mother and to every other mother in the district. To Begw, it was a marvel, this thing that was water on the milkshed table at night and a solid shiver in the morning, but more than that, something so good to eat. Last night, her mother had made an individual red one in a long-stemmed glass that she could take to the heather mountain for a tea party with Mair next door. Mair’s mother intended to make something for her too, she said. Begw was hoping that Mair’s mother would keep her word, because her possessive eagerness for the jelly was so great that she couldn’t think of sharing it with anyone. Her mother had understood that and made it individually in this pretty glass.

“This is all mine, isn’t it, mam?”

“Yes, every bit, you’ll have an appetite in the mountain air, and it will do you good.”

Doing you good was everything with her mother, not the nice sensation of feeling it slipping down her throat cold. But there was something else as well—she could lord it over Mair on account of the jelly. Mair had lorded it enough in school with her tomatoes, and said that they were the ones who’d had the first tomatoes in the district, and before anyone in town for that matter, and Robin had asked her how she could know that when there were tens of thousands of people living in the town.

“And here’s a bit of bread-and-butter for you to eat with it, and cold tea in a bottle. And you may get out your best shoes today for once, instead of you dragging those great clogs.”

“Oh, I’ll be just the same as Mair next door now.”

“Only on your feet, I hope.”

{191} Mrs. Huws the Preacher and Mair were at the gate when Begw and her mother went out.

“Indeed,” Mrs. Huws said, “I don’t know is it wise to let two eight-year-old girls go on their own to the mountain.”

“They won’t be on their own if they’re with each other,” her neighbour said, to be contrary.

“But what if some old tramp were to attack them, would two be much better than one?”

“It isn’t on the mountain tramps beg for charity, Mrs. Huws.”

“There are plenty of them crossing the mountain when their shoes are too bad to walk the roads. And there are a lot of wicked old boys around.”

“I’ve never seen wicked boys,” Begw’s mother said, as if her son Robin were an angel.

“Oh well, nothing to do but hope for the best. We ought to go with them,” [said] Mrs. Huws.

There wouldn’t be any fun in that, Begw thought, and for fear Mrs. Huws would carry out her suggestion, she started off, with Mair in her wake. They’d had permission to go bareheaded, since it was to the mountain they were going, with their hair ribbons like butterflies on the sides of their heads. Begw was wearing a pinafore with the two frills on its shoulders opening out like a fan. She noticed that Mair didn’t have a sign of food anywhere. She was carrying a baby doll on her arm, and that was all. Now perplexity began for Begw, that perplexity that would time and again become her lot. What was it best to do? She couldn’t eat her jelly and her bread-and-butter and look at Mair beside her without anything, and she was determined that she wouldn’t get any of her jelly. She could offer her bread-and-butter and a drink.

“I have jelly,” she said carefully.

“Tut, there’s nothing at all in that. It’s a cheap old thing. I’d rather have tomatoes.”

“Do you have some with you?”

“No.”

Begw’s face fell. She was afraid she would have to share her jelly.

A little bit before turning to the mountain, who did they see on the road but Winni Ffinni Hadog, standing with her arms spread as if she were doing drill.

“You can’t pass,” she said defiantly.

And the other two were trying to escape past her, but Winni’s two arms {192} were down on them like the two arms of a wooden soldier. Then she was taking each of them by the free hand and turning them around.

“I’m coming with you to the mountain,” she said.

“Who said that you could come?” Mair said.

“How do you know that we’re going to the mountain?” was Begw’s question.

“If you knew me, you wouldn’t be asking such a question.”

“Is it true you’re a witch?” Begw said.

“A little girl like you shouldn’t ask questions.”

Begw looked at her. She was wearing an old heavy frock, and a raggedy-looking pinafore with no shape to it, just two armholes and a neck-hole, with a drawstring through it. Her hair in long hanks on her head, and falling into her eyes. They had turned to the mountain by now, and a light breeze was running across the cotton grass, blowing Mair’s light frock and showing the needlework on her white petticoat. It flaunted the fallen hem of Winni’s frock from side to side like the tail of a cow in heat. Her clog struck a stone.

“Damn,” she said quietly, and then more loudly, “isn’t it a strange thing that you see stars when you bump your clog on a stone?”

Begw couldn’t believe her ears, and she didn’t hear Mair express surprise or protest, she decided that she hadn’t heard the swear-word. Moreover, the perplexity about sharing the jelly was becoming urgent. Now she would have to offer something to Winni.

“I’m clean worn out, I need food,” Winni said, pulling her hands from the hands of the other two.

“This patch of grass is made for our hide-out.” And she sat down on a patch of green grass in the middle of the heather.

“Now sit,” she said like an army officer.

The other two could only obey, as if they’d been spellbound.

“Have you ever been to Anglesey?” Winni said, looking towards that island.

“I’ve been, on the little steamer,” Mair said.

“I’ve never been,” Begw said.

“Nor I,” Winni said, “but I mean to go some day.”

“Where will you find the money?” Mair asked.

“I’m going into service, after I leave school next month.”

“Where?” Begw asked.

“I don’t know. But I’d like to go to London, quite far away.”

“Wouldn’t you feel homesick for your father and your mother?”

{193} “No, I don’t have a proper mother, and I have a devil of a father.”

Mair shut her eyes and then opened them in disdain. Begw made a sound like the sound of laughter in her throat, looking half-admiringly at Winni.

“God is going to put you in the big fire for swearing,” Mair said.

“No fear of that. God is kinder than your father, and more sensible than the fool of a father I have.”

“Oh,” Mair said, frightened, “I’ll tell Dada.”

“How many da’s do you have then?”

“Dada is what she calls her father, and I call mine ‘dad,’ Begw said.

“And I call mine numbskull,” Winni said.

“What’s ‘numbskull’?”

“A dimwitted man who thinks he has more sense than anyone. If he’d had sense, he wouldn’t have married her nibs there.”

“She isn’t your mother then?”

“No, my mother’s dead, and this one’s his second wife. My mother was a fool as well. An innocent fool, of course.”

“Oh,” Begw said, “why are you saying a thing like that about your mother?”

“Well, she was foolish to marry a man like dad to start with, and after she married him, to put up with everything from from. It was good that the poor thing could go to her grave. But Mister Mostyn has a master now.”

“Who’s Mister Mostyn?”

“I don’t know at all. Some quarry steward, for sure.”

Begw sighed, and looked at Winni’s face. Her face was red by now, and she was looking over the heads of the two smaller ones in the direction of the sea. Her mouth was crooked by nature, and since she was forced to fling her head back to toss her hair away from her eyes, she had a defiant look. As Begw was thinking when they might start on their tea, Winni started again.

“Do you dream sometimes?”

“Yes, at night,” Begw said.

“Oh no, it’s in the day I mean.”

“You can’t dream without sleeping.”

“I can,” Winni said.

“Don’t listen to her telling lies,” Mair said.

But Begw was listening with her mouth open, with Winni like a kind of prophet to her by now, looking just like the picture of Daniel in the lions’ den.

“I don’t do anything but dream all day,” Winni said. “That’s why I have {194} holes in my stockings, and that’s the reasons my father’s wife complained to him about me before he took his food tin out of his pocket after arriving home from the quarry. And I got my arse whipped before going to bed.”

“Oh-oh-oh,” Mair said, in horror.

Begw laughed nervously.

“It wasn’t a laughing matter to me. But one night I turned on him, and I fetched him a clout on the ear. I’m almost as tall as he is by now.”

“And what did he do?”

“Locked me in the bedroom without light or anything, and I didn’t get any supper. But I’d got to pay him in his own coin. But I didn’t sleep much because my stomach was ravenous.”

“Whats ‘ravenous’?”

“Thousands of lions roaring for food in your belly. But I mean to escape some day to London. I’ve started escaping today, because Lisi Jên threatened me with a thrashing this morning.”

“Who is Lisi Jên?”

“only my father’s wife.”

“How could I know?”

“There then, you know now.”

Mair was looking down at her frock without saying anything , and it was Begw asked the questions. She was hurt by the last answer.

Winni went on.

“Mind you,” she said, grinding her teeth, “I’ll be going like the breeze some day, and I won’t stop until I’m in London. And I’ll find a place to serve and I’ll make money.”

“Maids don’t make much money,” Mair said.

“Oh, it isn’t snobs like you I’m going to serve, but Queen Victoria herself. And I’ll have a white starched cap on my chignon, and a white apron, and long strings down to the hem of my skirt to tie it. And I’ll have a silk frock for going out at night and a gold bracelet, and a gold watch on my breast fastened to a bow-knot brooch and a great gold chain in two twists around my neck. And I’ll have a handsome sweetheart with curly hair, not one like these common old boys around here. And farewell to Twm Ffinni Hadog and his wife forever and ever.”

Then she began to pluck a tendril of the staghorn that was growing tightly twined around the stalks of the heather. She patiently plucked and plucked with her tough hand, and then, when she had enough, she put it around her head like a wreath.

“Here you are—the Queen of Sheba,” she said.

{195} At that, here she was, flinging off her two clogs and beginning to dance on the heather, her black heels looking like the heads of two jackdaws through the holes in her stockings. She was dancing like a wild thing, flinging her arms about, and turning her face towards the sun. She grasped the hem of her skirt with one hand and held the other arm up. Begw noticed that there was only the bare skin of oher haunches to be seen under her skirt. Presently she stopped, and fell half-sprawling on the ground.

“Oh, I’m dizzy.”

“Have a sip of cold tea, Winni,” Begw said, “this will do you good.”

She had got the world “Winni” out at last, and had moved a step forward in sympathy for her.

At that, Winni sat up.

“Give me that basket, I haven’t had a morsel of dinner.”

And like a person who’s lost her senses, she went and took the jelly glass and the spoon and gulped it all down, and then scoffed the slices of bread-and-butter. Begw was nailed to the earth, and the tears leapt into her eyes. Mair was smiling coldly.

“And now,” Winni said, getting up and tossing the glass into the basket, “I mean to thrash you.”

Mair ran for her life, and let her doll fall somewhere. Begw couldn’t move, only look into Winni’s face with her expression pleading for mercy. But Winni carried out her threat unceremoniously. She lifted up her clothes and thrashed her. Begw screamed, and managed to escape. She ran up the mountain crying, turned her eyes back once and saw Winni running with all her might after Mair. On and on Begw went, her body strangely light, until she reached an iron stile. Over the stile, and reaching wide level moorland. Going on running and finding herself going downwards. A valley came in sight, with a river running through it. And she stopped, and sat down on some nice moss. She was still hiccuping and crying, and another great gasp started as she remembered her shame. She was wretched in thinking that someone besides her mother had thrashed her. Then another feeling came, thinking how she’d begun to see something that she could like in Winni, instead of being like everybody in the district, shutting her out like a disreputable dungheap that no one could touch except with a manuring fork. And all of a sudden Winni went and did a thing that proved it was the people of the district who were right. She stopped crying, and a quiet sadness came over her. She lay at full length on the warm earth, and looked at the blue sky that was like a great parasol above her head. From the corner of her eye she could see a corner of Lake Llyncwel like a {196} fragment of the map of Ireland, and she felt angry at the beak of the mountain that was hindering her from seeing more. A nice feeling came over her, how nice it was to be apart, instead of being among people. There was Winni, had started to be likeable, but no, she had to forget her. This silence was nice. Every sound, it was a sound from far away, the sound of stones going down over the quarry tip, the sound of shot-firing from Llanberis, ther bleat of a lonely sheep far off somewhere, with all of it making her think of the baby’s sigh as he slept in his cradle at home. She went to sleep delighting in her surroundings. Then she heard a noise close by, and someone walking soft as velvet over the earth. She sat up quickly and saw her brother Robin coming towards her, with the food basket in his hand. She was almost cross with him for breaking in or her stillness.

“Well,” Robin said, “I had a fright.”

“Why?”

“Thinking you were lost. Better come home at once, or mom will start looking for you.”

“She isn’t expecting me now.”

“She will be, because I’ve sent Mair home on her own. I caught Winni Finni Hadog before Mair got a thrashing.”

“Where is she?”

“Who?”

“Winni.”

“She’s gone home.”

“I don’t think so, because she was saying she’d begun to escape from home today.”

“That’s an old song with Winni, she’d bound to arrive home before night, I can tell you.”

Neither of the two said a word on the way home about the trouble, Begw from shame, and Robin for once understanding his sister’s feelings. When they arrived, their mother was standing by the gate with Mrs. Huws and Mair, an I-told-you look on Mrs. Huws, and a very anxious look that turned into a welcoming smile on the face of her mother.

“That Winni should be put under lock and key somewhere,” Mrs. Huws said. “She’s much too old for her age, she isn’t fit to be among children.”

“Perhaps our own children wouldn’t be much better if they’d been raised like her, Mrs. Huws. The girl never had a chance with such a father, her mother was a proper woman.”

“Hm,” Mrs. Huws said, “they’re good-for-nothings, the lot of them. Like goes to like.”

“<em>You</em> of all people should know, Mrs. Huws,” Begw’s mother said with her strongest emphasis, “that it was the grace of God sent you to Trefriw Wells to Mr. Huws, and not Twm Ffinni Hadog.”

Then she took hold of Begw’s hand and pulled her through the gate, and she told Robin when Mrs. Huws and Mair were turning towards their house:

“Better thank Mrs. Huws for the privilege of saving Mair from the clutches of Twm Finni Hadog’s daughter.”

And the doors of the two houses were shut.

But after she reached the house and could sit in the chair, Begw’s mind began working on what she’d heard her mother say about Winni’s mother. “A proper woman.” The same sympathy for Winni as she’d had on the mountain came back to her, when she was talking about her day-dreams. A dream came to her too. She would like to go looking for Winni and have her mother ask her to come to tea with plenty of jelly, so that she could hear her talk. Her mother would be please as well to hear her talk and call people snobs. She could still see Winni’s face as it was when she talked about getting to go into service to the Queen. She couldn’t forget that face.

</xml>
