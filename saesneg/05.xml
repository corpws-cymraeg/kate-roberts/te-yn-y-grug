<xml>
<chapter>A Visitor for Tea</chapter>

{198} “Do you like Winni, mam?” Begw said, a few days after that strange tea party up on the mountain.

“Which Winni?”

“You know, Winni who went and ate my jelly up on the mountain that day.”

“Oh, Winni Ffinni Hadog.”

“Yes.”

Begw hadn’t been certain enough of her mother’s mood to take a chance on using the nickname.

“No, I don’t rightly know Winni. I knew her mother quite well. Why, what was it?”

“I’ve been thinking.”

“Thinking about what?”

“About Winni.”

“What about her?”

“I don’t know. Thinking I would—I would—. What are good-for-nothings, mam?”

“Oh, disreputable sort of people.”

“What’s ‘disreputable’?”

“Oh dear, what’s plaguing the girl? Do you think that I’m Charles’s Dictionary?”

Begw was silent for a while, brooding into the grate while her mother was mending work clothes. On seeing her like this, her mother began to think she had better try to get to the bottom of these perplexing questions, and in doing so, she perceived that she didn’t rightly know herself what “disreputable” and “good-for-nothings” were. Before she found a definition to satisfy her, here was another question, like a thunderbolt.

“Are we good-for-nothings, mam?”

{199} “Dear Lord, I should hope not.”

“Is Winni good-for-nothings?”

“She can’t be good-for-nothings, only a good-for-nothing. No, I don’t think Winni is a good-for-nothing.”

“Mrs.\ Huws the Minister said she was, and her family.”

“Everybody’s good-for-nothings to Mrs.\ Huws, except herself and her husband and Mair.”

“We are good-for-nothings then?”

“Perhaps to Mrs.\ Huws. But nobody else would call us good-for-nothings or disreputable.”

“Would people call Winni’s family disreputable?”

“Some people perhaps, but it isn’t for people to say.”

“Who then?”

“Well, godly people like Mrs.\ Huws next door are the ones who say, but it’s God should say. It’s He that knows and rules the world.”

“What’s ‘rules’?”

“Looking after things.”

“Well, God isn’t looking after things very well, is He?”

“Don’t talk that way, the fault is with people.”

“Who?”

“People like Mrs.\ Huws, next door, for being too good, and people like Twm Ffinni Hadog and his second wife for being lazy, and mean.”

“Why doesn’t God tell them to be the other way?”

“They don’t listen.”

“Are we good people, mam?”

“We’re trying to be.”

“Why do we want to try and be like Mrs.\ Huws then?”

“Drat it,” her mother said, “there you’ve made me plant this needle in my finger.”

Silence then, with only the hard sound of the needle going through the corduroy. But Begw hadn’t been able to square things in her mind. She didn’t know at all whether Winni was disreputable in her mother’s eyes or not, and because of that she wasn’t certain whether it would be a wise thing to ask her if Winni might come there to tea. She decided to venture on the hardest thing first.

“Mam?”

“Well?”

“May Winni come here to have tea some day?”

“I wouldn’t mind. Why do you want to have her?”

{200} “I like Winni.”

“After she thrashed you and ate your food?”

“Yes, but you see, mam, I liked her before that. And there were lions in her stomach, she said. They’re the ones ate the food, aren’t they?”

“Poor creature! There mustn’t be any regular meals there.”

After she had the promise, Begw felt it was safe enough to warn her mother about other things. She was very much afraid of her mother being disappointed in Winni.

“She swears like a cat, mind. She did on the mountain.”

“I’m sure she hears nothing else at home.”

“A pity, isn’t it?”

“A great pity.”

“She wants to go away, into service to Queen Victoria, she said.”

Her mother laughed without raising her head from the corduroy trousers.

“Where on earth would the little creature find clothes to go into service in town, not to mention London?”

“Oh, she doesn’t want to go to the town, she wants to go far away.”

“Well, in case she goes, you’d better try to get hold of her as soon as you can.”

This problem hadn’t dawned on Begw. It wasn’t possible to get hold of Winni at one of the meetings in chapel. She was like an eclipse of the sun in Robert Roberts of Holyhead’s almanac, “visible in this land every now and then”. Begw decided to go in the direction of her home. She wasn’t going to knock on the door. That would be just like going into a lions’ den. She might not come out of there alive. Next day she ventured as far as Winni’s house. It was an easy enough place to dawdle around without anyone seeing you, since there were some fifty yards of cart road between the gate and the house. Shivers of pleasure and fear were going down her back in turn as she neared the gate; she wanted to see Winni, and at the same time she was hoping that she wouldn’t see her this time, but that she’d see her the next time. But, indeed, after she reached the gate, she saw Winni close to the door of the house, holding a small child by the hand. She didn’t know what it would be best to do, whether to go towards her or call from the gate. Either one could upset Winni and send her into a flood of swear-words. She decided that her skin would stay healthier by standing at the gate.

“Oo—oo—,” she shouted.

{201} Winni raised her head, and gave it another toss back, to get her hair out of her eyes. She gazed in Begw’s direction for a long while, and then moved slowly towards her, pulling the baby by his hand—he was not too steady on his feet, and kept the far side of his body turned towards Winni as he walked pigeon-toed. Begw noticed there was a great heap of dung in front of the door of the cowshed, rising higher than the door, and she remembered her father’s description of a lazy man, “his dung-heap is taller than his cowshed door.”

“What do you want?” was Winni’s greeting from inside the gate.

“Mam is asking will you come for tea to our house tomorrow.”

Winni looked down her crooked mouth at Begw, as if she had asked her if she would come to Meeting. Then, quite majestically, as if she were Lord Newborough’s daughter, she asked:

“Where do you live?”

For a second Begw felt it was she who was living behind the dung-heap, and that Winni had changed places with Mair next door. But she took heart:

“Along the road that goes the other way from the chapel.”

“Close by the preacher’s house?”

“Yes, next door.”

Winni took time to consider, quite as if those houses were too low for her to accept an invitation to them.

“Is the preacher’s woman likely to come there, while we’re having tea?”

“That’s the last thing she would do.”

“All right, I’ll come then. But I don’t have any grand clothes,” she said patronizingly.

“There’s no need for grand clothes,” Begw said, unable to conceal her gladness or put on an act like Winni.

The baby began to push a filthy crust through the gate and offer it to Begw.

“No thanks, love,” Begw said. “Is he your brother?”

“Half-brother,” Winni answered, “but he’s quite a dear little thing.”

“What’s your name?”

“Sionyn,” he said and looked at his feet, shyly.

“Can Sionyn come with you tomorrow?” Begw asked.

“I don’t want him to come,” Winni said shortly. “When I go for a visit, I don’t want babies at my tail.”

Winni had the same clothes on as she’d had on the mountain. Perhaps {202} the pinafore had been washed, but it hadn’t been ironed. The baby had exactly the same sort of clothes, a frock and a shapeless pinafore, with the same raggedy look.

“Ta-ta, Sionyn.” She searched in her pocket, but there wasn’t a single lump of a sweet to give him.

And he smiled good-naturedly between the slats of the gate, and silently flung his crust to the other side.

“Ta-ta, Winni, and mind, we’ll be expecting you around three.”

“All right.” And quite unceremoniously Winni turned back without so much as a smile or a thank-you.

Next day Begw was on pins and needles all morning for fear her mother’s mood would change, and she’d say that Winni couldn’t come, fear that they wouldn’t have jelly, fear that Winni would swear so much that it would shock her mother. In a word, fear that she’d made a mistake in asking that she could come. She went to look around the milkshed in the morning when her mother was feeding the pigs, and found the jelly in a bowl on the floor, solidified, with a plate over it. When she saw her mother putting a cloth on the table after dinner, and taking out the griddle to get ready for making pancakes, she knew that everything was all right on that side. She had one terrible moment when her mother said, to herself more than anyone:

“Lobscouse would be a better meal for that girl, indeed, when she’s always famished.”

“You’re not going to make lobscouse for tea, are you?” Begw said, frightened, because she felt that even Winni Ffinni Hadog ought to have a tea like anyone else.

“Oh no, but I was thinking that it would do her more good than a tad of jelly, and nobody can eat much bread-and-butter with pancakes.”

“Wait until you see Winni eat,” Begw said to herself.

She didn’t leave her mother’s knee all the time she was making the pancakes. Robin said that he didn’t want to stay and have tea with Winni Ffinni Hadog, and Rhys began crying when he heard him say that. So Robin disappeared and took his little brother with him, a very unusual thing. The baby was sleeping in his cradle with his arms up in the warmth.

Presently they heard the sound of clogs on the doorstep, and Begw’s mother was there before her, saying:

“Come in, Winni,” welcomingly.

Winni was wearing the same clothes except that the pinafore was different, and the heels of her stockings were as if they’d been drawn together {203} with thread. Her face was very clean and shone, but the clean place ended precisely under her chin, in a black boundary line. The hanks of hair that were falling into her eyes on the mountain had been tied back with a scrap of calico. She stood on tiptoe on the doorstep, and then walked on tiptoe into the house.

“God, you have a clean place here,” she said. “Our house is like a stable.”

“You’d better come to the table now,” Begw’s mother said, interrupting her.

“You have jelly again—you must be having a tea party every day.”

“No,” Begw said, “this was made for you.”

“We never have a thing, we’re like Job on the dunghill…”

“Tut, there isn’t much of anything in it besides water,” Elin Gruffydd said.

“I haven’t had a pancake since mam was alive,” Winni said. “Lisi Jên never makes treats.”

“Who is Lisi Jên?” Begw’s mother asked.

“My father’s wife. She isn’t my mother, mercifully. I’d be ashamed to be related to her.”

“Well, you ought to respect her,” Elin Gruffydd said, “since she married your father.”

“Respect, indeed. How can you respect a slut? She’s an old devil…”

Begw began to tremble, for fear the swearing would get worse.

Winni went ahead.

“I’d be able to live with her all right, if she’d let me clean. But the house is so filthy that she’s afraid of my getting a good look at it, and she won’t let me. The pigsty is cleaner than the house.”

“But Winni, can’t you tidy yourself up a bit?”

“I’m the one washed this pinafore this morning, and put it on the gorse to dry, but I had to do it on the sly, or I wouldn’t have had soap. Oh, these pancakes are good.”

“Have some more.” And Elin Gruffydd lifted another three on the fork. That’s the ninth, Begw said to herself.

“And my father’s a real numbskull. He’s besotted with Lisi Jên. If mam had been filthy like that she’d have got a thrashing from him. But Lisi Jên can do no wrong.”

“How long is it since they were married?”

“Some two years. It wasn’t much more than a year after mam died.”

“Come, one more helping, Winni.”

And she took another three pancakes.

{204} “Lisi Jên won’t get up to make breakfast for him before he goes to the quarry. She’ll lie nicely in bed until nine. And he doesn’t complain that he has to make his breakfast. Mam got up until she wasn’t able, and she’d moan in pain as she sliced bread-and-butter to put in his food tin, with him saying: ‘What the devil’s wrong with you?’ I’d get up sometimes and make a fire but I couldn’t slice bread-and-butter.”

The talk was going in a different direction than Begw had hoped. Winni wasn’t defiant as she’d been on the mountain, and there wasn’t a sign of dancing on her today.

“Can’t you come to chapel sometimes, Winni?” Begw’s mother asked.

“I haven’t any clothes, and I don’t want to come near any old snobs like this woman next door.”

“Everybody isn’t a snob, you know.”

“Everybody turns their nose up at me as if I were dirt. Their houses are dovecotes, a lot of them, too.”

“You want to come and pay them no mind. They aren’t any better than you.”

“No, honest to God, I wouldn’t look through a quill at some of them. There’s Lisi Jên’s aunt, with her feather boa and her morocco shoes and jewels like pegs for pigs in her ears, and she’s living in debt.”

“And she goes to town every Saturday,” Begw said.

“How do you know?” Winni said.

“I get a halfpenny from her for carrying her parcels from the brake.”

“That’s her to a T, a halfpenny for you, nothing for the shop in the village here, and everything to the shops in town. That’s what you mean by a ‘lady’.”

Elin Gruffydd laughed.

“That’s the only time I like Lisi Jên, when her aunt turns up her nose at her. Taking a photo of Lisi Jên and her together would make a good picture.”

Winni laughed, for the first time since she’d arrived.

“Snobs, that’s what most of the people are here, and they can look quite respectable on Sunday in chapel. But it’s a pity you couldn’t see them up on that mountain at night.”

Elin Gruffydd thought that she’d better interrupt at this point.

“When will you be leaving school, Winni?”

“A bit before Christmas, I’ll be thirteen then. And I want to go to London into service—go quite far away.”

“Wouldn’t you rather go to town or somewhere closer to home? You need to have very grand clothes to go to London.”

{205} “London or nothing for me. I could wash dishes with a starched white cap on my head. There are great cellars in London with gas lighting them, and things like a box carrying the food up to the high-and-mighty without anyone carrying it. And I’d have an evening off, and I’d go to chapel then. Nobody would know me there, and nobody would know I’m Twm Ffinni Hadog’s daughter.”

“How do you know all these stories about London, Winni?” Begw’s mother said, putting another spoonful of jelly on her plate.

“I’ve read about them, on the sly. I’d know more if Lisi Jên weren’t after me like a leech. It’s in bed at five in the morning I have the best chance, and I hide the book under the chaff-bed. No danger of Lisi Jên finding it there. She never makes the bed.”

“Can you read English?” Begw asked.

“A little bit, enough to understand what sort of place London is.”

Begw was staring at her with admiration, and her mother with compassion.

“You see,” Winni went on, “if I were to go into service in town, I know how it would be. My father would be coming down to borrow my wages shilling by shilling to get drink for that hogshead of a belly of his, and I wouldn’t see a halfpenny. Another thing, they’re snobs in the town as well. Flies hatched from a dung-heap, that’s what they are, the same as Lisi Jên’s aunt.”

“Maybe you’ll be homesick after going to London,” Begw ventured cautiously. Winni was silent for a moment, gazing earnestly at her plate.

“Yes, I’d be homesick for someone, that’s Sionyn. He’s a dear little old thing, but no one takes any notice of him but me. Your little baby is heavenly clean, and Sionyn’s like a dung-heap. He never gets to go to the road; I’d be able to see him sometimes if I went to the town.”

“You want to insist on being allowed to wash his clothes and your own clothes, Winni, no matter what your mother says, so that you can go around looking nice. Will you have more pancakes?”

“I’ll finish with bread-and-butter. This will make a feast for me for a month. A pity that Sionyn couldn’t have had a bite.”

“I did ask you to bring him,” Begw said.

“I’ll give you a few pancakes to take to him,” Elin Gruffydd said.

“I don’t dare,” Winni said, “or I’ll get a thrashing from Lisi Jên for going around gossiping.”

Then she raised her head suddenly, as they heard footsteps in the courtyard. Before they could clear their throats, there was Winni’s stepmother, Lisi Jên, inside the house, shouting without taking notice of anyone.

{206} “Here’s where you are, yes, going around filling your belly, when I had nobody to look after this child.” (Sionyn was on her arm.) “Come home this minute, so you can empty the chamber pots. Shame on you, Elin Gruffydd, for letting a thing like this into your house.”

“Sit down,” Elin Gruffydd said quite deliberately, taking hold of her, and steering her towards a chair. “You’ll have a cup of tea and a pancake now. I’ll make fresh tea in the teapot.”

Lisi Jên went like a lamb and sat on the chair.

“Winni,” Sionyn shouted, and ran to his half-sister. And she placed him on her knee, and began to feed him from her own plate. In no time they were beginning once again to have tea, but the atmosphere was quite different. Everybody looking very serious, except for Sionyn, who was getting his food from Winni’s hand like a chick. It was obvious that her stepmother’s blackguarding had had no effect on the latter, because she was looking quite happy as she dandled Sionyn and fed him. She couldn’t bring the pancakes to his mouth fast enough for him.

After they finished eating, everybody got up.

“Thank you,” Lisi Jên said, quite brusquely.

Begw thought from her bearing that Winni meant to make a prophetic speech before leaving, but all that she said was:

“Thank you very much, Elin Gruffydd, that was the best meal I ever had. It will have to do us for a long while.”

She said this looking at her stepmother, whose own look said: “Wait till you reach home, you’ll get ‘the best meal’.”

As they turned from the door, Elin Gruffydd thought that “slut” was the proper word for Lisi Jên. On seeing Sionyn’s good-natured smile, she remembered that she had sweets in the drawer, and ran to fetch them and give them to him.

Elin Gruffydd went to escort them to the gate, and of course, Mrs.\ Huws the Minister had to be in the garden, weeding where there weren’t weeds and seeing who the visitors were. But Elin Gruffydd paid no mind. There was something besides pleased or displeased thoughts in her heart, they were depressed thoughts as she saw the three turn towards the road. “Why?” she said to herself, “why?” She waved her hand at Sionyn.

After going into the house Begw held inquest on the visit. It hadn’t been quite as she’d thought it would be. She had thought that Winni’s eloquence would rise to the same heights as it had on the mountain, or higher. But Winni had been very flat. Perhaps she had made her depressed in talking about homesickness. Leaving home wasn’t an easy thing, even for Winni who hated it. She wondered if she dared ask her mother a question.

{207} Elin Gruffydd too was sitting, reflecting, with the baby on her knee suckling.

“Did you like Winni, mam?”

“Yes, the girl is all right, if she could get fair play. And that little Sionyn, dear lad.”

“Winni’s nice to him, isn’t she?”

“Yes, nobody else is, obviously. But Mrs.\ Huws next door is right about one thing.”

“What’s that?”

“Winni is much older than her age. But it’s easy enough for us to talk. She’s never been a child, obviously. It will be an achievement for her to get free from the clutches of Lisi Jên and her father. But if I know people, Winni is sure to find a way of being able to look after herself some day.”

“And after Sionyn, won’t she, mam?”

“Yes.”

“Maybe God looks after them better than we think.”

“Maybe.”

But the great thing to Begw was that her mother liked Winni. She hadn’t made a mistake in asking if Winni might come to tea.
</xml>
